# Lithuanian translation for json-glib.
# Copyright (C) 2012 json-glib's COPYRIGHT HOLDER
# This file is distributed under the same license as the json-glib package.
# Aurimas Černius <aurisc4@gmail.com>, 2012, 2013, 2014, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: json-glib master\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=json-"
"glib&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-03-18 16:05+0000\n"
"PO-Revision-Date: 2017-04-24 23:07+0300\n"
"Last-Translator: Aurimas Černius <aurisc4@gmail.com>\n"
"Language-Team: Lietuvių <gnome-lt@lists.akl.lt>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n"
"%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: json-glib/json-glib-format.c:50
msgid "Prettify output"
msgstr "Graži išvestis"

#: json-glib/json-glib-format.c:51
msgid "Indentation spaces"
msgstr "Įtrauka tarpais"

#. Translators: the first %s is the program name, the second one
#. * is the URI of the file, the third is the error message.
#.
#: json-glib/json-glib-format.c:77 json-glib/json-glib-validate.c:63
#, c-format
msgid "%s: %s: error opening file: %s\n"
msgstr "%s: %s: klaida atveriant failą: %s\n"

#. Translators: the first %s is the program name, the second one
#. * is the URI of the file, the third is the error message.
#.
#: json-glib/json-glib-format.c:89 json-glib/json-glib-validate.c:75
#, c-format
msgid "%s: %s: error parsing file: %s\n"
msgstr "%s: %s: klaida skaitant failą: %s\n"

#. Translators: the first %s is the program name, the
#. * second one is the URI of the file.
#.
#: json-glib/json-glib-format.c:108
#, c-format
msgid "%s: %s: error writing to stdout"
msgstr "%s: %s: klaida rašant į standartinę išvestį"

#. Translators: the first %s is the program name, the second one
#. * is the URI of the file, the third is the error message.
#.
#: json-glib/json-glib-format.c:129 json-glib/json-glib-validate.c:87
#, c-format
msgid "%s: %s: error closing: %s\n"
msgstr "%s: %s: klaida užveriant: %s\n"

#: json-glib/json-glib-format.c:158 json-glib/json-glib-validate.c:115
msgid "FILE"
msgstr "FAILAS"

#. Translators: this message will appear after the usage string
#. and before the list of options.
#: json-glib/json-glib-format.c:161
msgid "Format JSON files."
msgstr "Formatuoti JSON failus."

#: json-glib/json-glib-format.c:162
msgid "json-glib-format formats JSON resources."
msgstr "json-glib-format formatuoja JSON resursus."

#. Translators: the %s is the program name. This error message
#. * means the user is calling json-glib-validate without any
#. * argument.
#.
#: json-glib/json-glib-format.c:179 json-glib/json-glib-validate.c:136
#, c-format
msgid "Error parsing commandline options: %s\n"
msgstr "Klaida skaitant komandų eilutės parametrus: %s\n"

#: json-glib/json-glib-format.c:181 json-glib/json-glib-format.c:195
#: json-glib/json-glib-validate.c:138 json-glib/json-glib-validate.c:152
#, c-format
#| msgid "Try \"%s --help\" for more information."
msgid "Try “%s --help” for more information."
msgstr "Daugiau informacijos gausite įvykdę „%s --help“."

#. Translators: the %s is the program name. This error message
#. * means the user is calling json-glib-validate without any
#. * argument.
#.
#: json-glib/json-glib-format.c:193 json-glib/json-glib-validate.c:150
#, c-format
msgid "%s: missing files"
msgstr "%s: trūksta failų"

#. Translators: this message will appear after the usage string
#. and before the list of options.
#: json-glib/json-glib-validate.c:118
msgid "Validate JSON files."
msgstr "Tikrinti JSON failus."

#: json-glib/json-glib-validate.c:119
msgid "json-glib-validate validates JSON data at the given URI."
msgstr "json-glib-validate patikrina JSON duomenis ties pateiktu URI."

#. translators: the %s is the name of the data structure
#: json-glib/json-gobject.c:943
#, c-format
#| msgid "Expecting a JSON object, but the root node is of type `%s'"
msgid "Expecting a JSON object, but the root node is of type “%s”"
msgstr "Tikimasi JSON objekto, bet šakninė viršūnė yra „%s“ tipo"

#. translators: the '%s' is the type name
#: json-glib/json-gvariant.c:523
#, c-format
#| msgid "Unexpected type '%s' in JSON node"
msgid "Unexpected type “%s” in JSON node"
msgstr "Netikėtas tipas „%s“ JSON viršūnėje"

#: json-glib/json-gvariant.c:593
msgid "Missing elements in JSON array to conform to a tuple"
msgstr "Trūksta elementų JSON masyve junginiui sudaryti"

#: json-glib/json-gvariant.c:621
#| msgid "Missing closing symbol ')' in the GVariant tuple type"
msgid "Missing closing symbol “)” in the GVariant tuple type"
msgstr "Trūksta uždarančio simbolio „)“ GVariant junginio tipe"

#: json-glib/json-gvariant.c:629
msgid "Unexpected extra elements in JSON array"
msgstr "Netikėti papildomi elementai JSON masyve"

#: json-glib/json-gvariant.c:908
msgid "Invalid string value converting to GVariant"
msgstr "Netinkama eilutės reikšmė konvertavimui į GVariant"

#: json-glib/json-gvariant.c:964
msgid ""
"A GVariant dictionary entry expects a JSON object with exactly one member"
msgstr "GVariant žodyno įrašas tikisi JSON objekto su vieninteliu nariu"

#: json-glib/json-gvariant.c:1248
#, c-format
#| msgid "GVariant class '%c' not supported"
msgid "GVariant class “%c” not supported"
msgstr "GVariant klasė „%c“ nepalaikoma"

#: json-glib/json-gvariant.c:1296
msgid "Invalid GVariant signature"
msgstr "Netinkamas GVariant aprašas"

#: json-glib/json-gvariant.c:1344
msgid "JSON data is empty"
msgstr "JSON duomenys yra tušti"

#. translators: %s: is the file name, the first %d is the line
#. * number, the second %d is the position on the line, and %s is
#. * the error message
#.
#: json-glib/json-parser.c:907
#, c-format
msgid "%s:%d:%d: Parse error: %s"
msgstr "%s:%d:%d: skaitymo klaida: %s"

#: json-glib/json-parser.c:990
msgid "JSON data must be UTF-8 encoded"
msgstr "JSON duomenys turi būti koduoti UTF-8"

#: json-glib/json-path.c:389
msgid "Only one root node is allowed in a JSONPath expression"
msgstr "JSONPath išraiškoje leidžiamas vienintelė šakninė viršūnė"

#. translators: the %c is the invalid character
#: json-glib/json-path.c:398
#, c-format
#| msgid "Root node followed by invalid character '%c'"
msgid "Root node followed by invalid character “%c”"
msgstr "Po šakninės viršūnės yra netinkamas simbolis „%c“"

#: json-glib/json-path.c:438
msgid "Missing member name or wildcard after . character"
msgstr "Trūksta nario pavadinimo arba pakaitos simbolio po . simbolio"

#: json-glib/json-path.c:512
#, c-format
#| msgid "Malformed slice expression '%*s'"
msgid "Malformed slice expression “%*s”"
msgstr "Blogai suformuota dalinimo išraiška „%*s“"

#: json-glib/json-path.c:556
#, c-format
#| msgid "Invalid set definition '%*s'"
msgid "Invalid set definition “%*s”"
msgstr "Netinkamas aibės apibrėžimas „%*s“"

#: json-glib/json-path.c:609
#, c-format
#| msgid "Invalid slice definition '%*s'"
msgid "Invalid slice definition “%*s”"
msgstr "Netinkamas dalinimo apibrėžimas „%*s“"

#: json-glib/json-path.c:637
#, c-format
#| msgid "Invalid array index definition '%*s'"
msgid "Invalid array index definition “%*s”"
msgstr "Netinkamas masyvo apibrėžimas „%*s“"

#: json-glib/json-path.c:656
#, c-format
#| msgid "Invalid first character '%c'"
msgid "Invalid first character “%c”"
msgstr "Netinkamas pirmasis simbolis „%c“"

#: json-glib/json-reader.c:474
#, c-format
#| msgid ""
#| "The current node is of type '%s', but an array or an object was expected."
msgid ""
"The current node is of type “%s”, but an array or an object was expected."
msgstr "Dabartinė viršūnė yra „%s“ tipo, bet tikėtasi masyvo arba objekto."

#: json-glib/json-reader.c:486
#, c-format
#| msgid ""
#| "The index '%d' is greater than the size of the array at the current "
#| "position."
msgid ""
"The index “%d” is greater than the size of the array at the current position."
msgstr "Indeksas „%d“ yra didesnis nei masyvo dydis dabartinėje pozicijoje."

#: json-glib/json-reader.c:503
#, c-format
#| msgid ""
#| "The index '%d' is greater than the size of the object at the current "
#| "position."
msgid ""
"The index “%d” is greater than the size of the object at the current "
"position."
msgstr "Indeksas „%d“ yra didesnis nei objekto dydis dabartinėje pozicijoje."

#: json-glib/json-reader.c:587 json-glib/json-reader.c:751
#: json-glib/json-reader.c:802 json-glib/json-reader.c:840
#: json-glib/json-reader.c:878 json-glib/json-reader.c:916
#: json-glib/json-reader.c:954 json-glib/json-reader.c:999
#: json-glib/json-reader.c:1035 json-glib/json-reader.c:1061
msgid "No node available at the current position"
msgstr "Nėra viršūnės dabartinėje pozicijoje"

#: json-glib/json-reader.c:594
#, c-format
#| msgid "The current position holds a '%s' and not an array"
msgid "The current position holds a “%s” and not an array"
msgstr "Dabartinėje pozicijoje yra „%s“, o ne masyvas"

#: json-glib/json-reader.c:670
#, c-format
#| msgid "The current node is of type '%s', but an object was expected."
msgid "The current node is of type “%s”, but an object was expected."
msgstr "Dabartinė viršūnė yra „%s“ tipo, bet tikėtasi objekto."

#: json-glib/json-reader.c:677
#, c-format
#| msgid ""
#| "The member '%s' is not defined in the object at the current position."
msgid "The member “%s” is not defined in the object at the current position."
msgstr "Narys „%s“ neapibrėžtas dabartinėje pozicijoje esančiame objekte."

#: json-glib/json-reader.c:758 json-glib/json-reader.c:809
#, c-format
#| msgid "The current position holds a '%s' and not an object"
msgid "The current position holds a “%s” and not an object"
msgstr "Dabartinė pozicija turi „%s“, o ne objektą"

#: json-glib/json-reader.c:849 json-glib/json-reader.c:887
#: json-glib/json-reader.c:925 json-glib/json-reader.c:963
#: json-glib/json-reader.c:1008
#, c-format
#| msgid "The current position holds a '%s' and not a value"
msgid "The current position holds a “%s” and not a value"
msgstr "Dabartinė pozicija turi „%s“, o ne reikšmę"

#: json-glib/json-reader.c:971
msgid "The current position does not hold a string type"
msgstr "Dabartinė pozicija turi „%s“, o ne eilutės tipą"
